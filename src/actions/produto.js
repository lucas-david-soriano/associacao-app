import axios from 'axios'
import config from '../../app.json'
import { LISTA_PRODUTO } from './types';

export const listaProduto = () => dispatch =>  {
  axios.get(`${config.api}produtos`).then(response => {
   console.log(response.data)
   dispatch({
       type: LISTA_PRODUTO,
       payload: response.data
     })
  }).catch(e => {
    console.log(e)
  })
}