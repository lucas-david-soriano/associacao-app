import { ADD_PLACE, TESTE } from './types';

export const addPlace = placeName => {
  return {
    type: ADD_PLACE,
    payload: placeName
  }
}

export const teste = text => {
  return {
    type: TESTE,
    payload: text
  }
}