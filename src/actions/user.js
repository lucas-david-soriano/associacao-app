import { ADD_PLACE, TESTE, LOAD_USERS } from './types';

import axios from 'axios'

export const addPlace = placeName => {
  return {
    type: ADD_PLACE,
    payload: placeName
  }
}

export const teste = text => {
  return {
    type: TESTE,
    payload: text
  }
}

export const loadUsers = () => dispatch =>  {
   axios.get('https://jsonplaceholder.typicode.com/users').then(response => {
    console.log(response.data)
    dispatch({
        type: LOAD_USERS,
        payload: response.data
      })
   })
   
}