import { LISTA_PRODUTO } from '../actions/types';

const initialState = {
  produtos: []
};


const produtoReducer = (state = initialState, action) => {
    switch(action.type) {
      case LISTA_PRODUTO:
        return {
          produtos: action.payload
        };
        default:
            return state;
    }
  }
  
  export default produtoReducer;



