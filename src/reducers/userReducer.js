import { ADD_PLACE, ISLOADING, LOAD_USERS } from '../actions/types';

const initialState = {
  users: [],
  isLoading: true
};

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case LOAD_USERS:
      return {
        users: action.payload,
      };
    case ISLOADING: 
      return {
        ...state,
        isLoading: action.payload
      }
    default:
      return state;
  }
}

export default userReducer;