import { ADD_PLACE, TESTE } from '../actions/types';

const initialState = {
  placeName: '',
  places: [
    {
        key: '1',
        value: 'AOOOBA'
    }
  ],
  teste: 'EAI TIO'
};

const placeReducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_PLACE:
      return {
        ...state,
        places: state.places.concat({
          key: Math.random(),
          value: action.payload + ' SALVE MANO'
        })
      };
    case TESTE: 
      return {
        ...state,
        teste: action.payload
      }
    default:
      return state;
  }
}

export default placeReducer;