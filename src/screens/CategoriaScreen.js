import React, { Component } from 'react';
import {
  Image,
  ScrollView, 
  View,
  Dimensions
} from 'react-native'

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    List,
    ListItem,
    Spinner
  } from 'native-base';

import axios from "axios"
import config from "./../../app.json"


class CategoriaScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        categorias: [],
        isLoading: true
    }
  }

  componentDidMount() {
    axios.get(`${config.api}categorias/${config.associacaoId}`).then(r => {
      this.setState({ categorias: r.data, isLoading: false })
    })
  }

  render() {
    return (
      <Container>
        <Header>
          <Left />
            <Body>
              <Title>Guia comercial</Title>
            </Body>
          <Right/>
        </Header>
        {this.state.isLoading ? <Spinner /> :
        <Content>
          <Text>{JSON.stringify(this.state.categorias)}</Text>
          {this.state.categorias.map((c, i) => {
            return(
              <ListItem key={i} icon onPress={() => this.props.navigation.navigate('LojasScreen',{'categoria': c.categoria_id})}>
                <Left>
                  <Button style={{ backgroundColor: "#007AFF" }}>
                    <Icon active name="wifi" />
                  </Button>
                </Left>
                <Body>
                  <Text>{c.categoria_id.nome}</Text>
                </Body>
                <Right>
                  <Icon active name="arrow-forward" />
                </Right>
              </ListItem>
            )
          })}
          
        </Content>
        }
      </Container>
    );
  }
}

export default CategoriaScreen