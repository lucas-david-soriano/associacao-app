import React, { Component } from 'react';
import {
  Image,
  ScrollView, 
  View,
  Dimensions
} from 'react-native'

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    List,
    ListItem,
    Spinner,
    CardItem
  } from 'native-base';

import Carousel from '../components/Carousel'
import axios from "axios"
import config from "./../../app.json"
import HTMLView from 'react-native-htmlview'

// <Text>{JSON.stringify(this.state.loja)}</Text>

class LojaScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            loja: {}
        }
    }
    
    async componentDidMount() {
        const { navigation } = this.props;
        const loja = navigation.getParam('loja', false)
        await this.setState({loja: loja, isLoading: false})
    }

    makeImagens(imagens) {
        let img = []
        if (imagens && imagens.length > 0) {
            for (let i = 0; i < imagens.length; i++) {
                img.push({
                    uri: `${config.base}empresas/${imagens[i]}`
                })
            }
        }
        return img
    }
    render() {
        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>{this.state.isLoading ? 'Carregando' : this.state.loja.nome}</Title>
                    </Body>
                    <Right/>
                </Header>
                {this.state.isLoading ? <Spinner /> : 
                    <Content>
                        <Carousel images={this.makeImagens(this.state.loja.imagens)} />
                        
                        <List>
                            <ListItem itemDivider>
                                <Text>Dados de {this.state.loja.nome}</Text>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="map-marker" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.state.loja.endereco.rua} {this.state.loja.endereco.numero}</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="map" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Bairro: {this.state.loja.endereco.bairro}</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="street-view" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>CEP: {this.state.loja.endereco.cep}</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="envelope-o" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Email: {this.state.loja.email ? this.state.loja.email : 'Não informado'}</Text>
                                </Body>
                            </ListItem>
                            <ListItem itemDivider>
                                <Text>Telefones</Text>
                            </ListItem>
                            <View>
                            {this.state.loja.telefones ? this.state.loja.telefones.map((t, i) =>{
                                return (
                                    <ListItem icon key={i}>
                                        <Left>
                                            <Button style={{ backgroundColor: "#FF9501" }}>
                                                <Icon type="FontAwesome" active name="phone" />
                                            </Button>
                                        </Left>
                                        <Body>
                                            <Text>{t}</Text>
                                        </Body>
                                    </ListItem>
                                )
                            }) : 
                                <ListItem>
                                    <Text>Nenhum telefone informado</Text>
                                </ListItem>
                            }
                            </View>
                            <ListItem itemDivider>
                                <Text>Redes Sociais</Text>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="facebook" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.state.loja.facebook_link ? 'Facebook' : 'Não informado'}</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="youtube-play" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.state.loja.youtube_link ? 'Youtube' : 'Não informado'}</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{ backgroundColor: "#FF9501" }}>
                                        <Icon type="FontAwesome" active name="external-link" />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.state.loja.site_link ? this.state.loja.site_link : 'Não informado'}</Text>
                                </Body>
                            </ListItem>
                        </List>
                    </Content>
                }
            </Container>
        );
    }
}

export default LojaScreen