import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableHighlight,
  AsyncStorage
} from 'react-native'
import { 
  Container, 
  Header, 
  Left, 
  Body, 
  Right, 
  Button, 
  Icon, 
  Title,
  Content,
  List, 
  ListItem,
  Text, 
  Spinner
} from 'native-base';

import { connect } from 'react-redux';

import config from '../../app.json'
import axios from 'axios'
import Carousel from '../components/Carousel'


class HomeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      associacao: {}
    }
  }

  componentDidMount() {
    axios.get(`${config.api}associacoes/${config.associacaoId}`).then(async (r) => {
      await AsyncStorage.setItem('associacao', JSON.stringify(r.data))
      this.setState({isLoading: false, associacao: r.data})
    })
  }

  makeImagens(imagens) {
    let img = []
    if (imagens && imagens.length > 0) {
        for (let i = 0; i < imagens.length; i++) {
            img.push({
                uri: `${config.base}associacoes/${imagens[i]}`
            })
        }
    }
    return img
  }

  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>{this.state.isLoading ? null : this.state.associacao.cidade_id.nome}</Title>
          </Body>
          <Right/>
        </Header>
        <Carousel images={this.makeImagens(this.state.associacao.imagens)} />
        {this.state.isLoading ? <Spinner /> :
        <Content>
          <ListItem itemDivider>
            <Text>Menu</Text>
          </ListItem>       
          <ListItem icon onPress={() => this.props.navigation.navigate('CidadeScreen')}>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>A cidade</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => this.props.navigation.navigate('AssociacaoScreen')}>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>Conheça a associação</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => this.props.navigation.navigate('NoticiasScreen')}>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>Notícias</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => this.props.navigation.navigate('EventosScreen')}>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>Campanhas e eventos</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => this.props.navigation.navigate('CategoriaScreen')}>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>Guia comercial</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
        }
      </Container>
    );
  }
}

const mapState = state => ({
  produtos: state.produtos.produtos
})

export default connect(mapState)(HomeScreen)