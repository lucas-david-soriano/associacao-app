import React, { Component } from 'react';
import {
  Image,
  ScrollView, 
  View,
  Dimensions,
  AsyncStorage
} from 'react-native'

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    Card,
    CardItem,
    Spinner
} from 'native-base';

import config from "./../../app.json"
import Carousel from '../components/Carousel'
import axios from "axios"
import HTMLView from 'react-native-htmlview'

class CidadeScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            cidade: {},
            isLoading: true
        }
    }

    async componentDidMount() {
        let associacao = await AsyncStorage.getItem('associacao')
        associacao = JSON.parse(associacao)
        this.setState({cidade: associacao.cidade_id, isLoading: false})
    }

    makeImagens(imagens) {
        let img = []
        if (imagens && imagens.length > 0) {
            for (let i = 0; i < imagens.length; i++) {
                img.push({
                    uri: `${config.base}cidades/${imagens[i]}`
                })
            }
        }
        return img
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>A cidade</Title>
                    </Body>
                    <Right/>
                </Header>
                {this.state.isLoading ?

                    <Spinner color='primary' /> :

                    <Content>
                        <Carousel images={this.makeImagens(this.state.cidade.imagens)} />
                        <Text>Conheça melhor a cidade</Text>
                        <HTMLView
                            value={this.state.cidade.descricao}
                        />
                    </Content>
                    }
            </Container>
        );
    }
}

export default CidadeScreen