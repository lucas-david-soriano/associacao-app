import React, { Component } from 'react';
import { Spinner, Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Title } from 'native-base';
import { View } from 'react-native'
import config from "./../../app.json"
import Carousel from '../components/Carousel'
import axios from "axios"
import HTMLView from 'react-native-htmlview'


export default class NoticiasScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
        noticias: [],
        isLoading: true
    }
  }

  componentDidMount() {
    axios.get(`${config.api}noticias/${config.associacaoId}`).then(r => {
      this.setState({ noticias: r.data, isLoading: false })
    })
  }

  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Notícias</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          {this.state.isLoading ? <Spinner /> :
          <List>
            {this.state.noticias.map((n, i) => {
              return(
                <ListItem key={i} avatar onPress={() => this.props.navigation.navigate('VerNoticiaScreen', {'noticia': n})}>
                  <Left>
                    <Thumbnail source={{ uri: `${config.base}noticias/${n.imagens[0]}` }} />
                  </Left>
                  <Body>
                    <Text>{n.titulo}</Text>
                    <View note>
                      <HTMLView
                        value={n.descricao}
                      />
                    </View>
                  </Body>
                  <Right>
                    <Text note>3:43 pm</Text>
                  </Right>
                </ListItem>
              )
            })}
          </List>
          }
        </Content>
      </Container>
    );
  }
}