import React, { Component } from 'react';

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    Card,
    CardItem
  } from 'native-base';

import config from "./../../app.json"
import Carousel from '../components/Carousel'
import HTMLView from 'react-native-htmlview'

class VerNoticiaScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    makeImagens(imagens) {
        let img = []
        if (imagens && imagens.length > 0) {
            for (let i = 0; i < imagens.length; i++) {
                img.push({
                    uri: `${config.base}noticias/${imagens[i]}`
                })
            }
        }
        return img
    }
    render() {
        const { navigation } = this.props;
        const noticia = navigation.getParam('noticia', false);
        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>{noticia ? noticia.titulo : 'Notícia não encontrada'}</Title>
                    </Body>
                    <Right/>
                </Header>
                {noticia ?
                    <Content>
                        <Carousel images={this.makeImagens(noticia.imagens)} />
                        <Card>
                            <CardItem>
                                <Body>
                                    <Text>{noticia.titulo}</Text>
                                </Body>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <HTMLView
                                    value={noticia.descricao}
                                />
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>
                : null }
            </Container>
        );
    }
}

export default VerNoticiaScreen