import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Title } from 'native-base';
export default class EventosScreen extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Eventos</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <List>
            <ListItem avatar onPress={() => this.props.navigation.navigate('VerEventoScreen')}>
              <Left>
                <Thumbnail source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBnHhgvPZNSsTc8psqvmRSsprBZhEn6SV7NkuKX9qg5R_sWeXq' }} />
              </Left>
              <Body>
                <Text>Cash Promotora</Text>
                <Text note>A Cash Promotora anuncia o maior evento de todos os tempos</Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}