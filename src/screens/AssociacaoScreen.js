import React, { Component } from 'react';
import {
    Image,
    ScrollView,
    View,
    Dimensions
} from 'react-native'

import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Content,
    Text,
    List,
    ListItem,
    Card,
    CardItem,
    Spinner
} from 'native-base';

import config from "./../../app.json"
import Carousel from '../components/Carousel'
import axios from "axios"
import HTMLView from 'react-native-htmlview'


class AssociacaoScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            associacao: {},
            isLoading: true
        }
    }

    componentDidMount() {
        axios.get(`${config.api}associacoes/${config.associacaoId}`).then(r => {
            this.setState({ associacao: r.data, isLoading: false })
        })
    }

    makeImagens(imagens) {
        let img = []
        if (imagens && imagens.length > 0) {
            for (let i = 0; i < imagens.length; i++) {
                img.push({
                    uri: `${config.base}associacoes/${imagens[i]}`
                })
            }
        }
        return img
    }

    render() {

        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>A associação</Title>
                    </Body>
                    <Right />
                </Header>
                {this.state.isLoading ?

                    <Spinner color='primary' /> :

                    <Content>
                        <Carousel images={this.makeImagens(this.state.associacao.imagens)} />
                        <Text>Conheça melhor a associação</Text>
                        <HTMLView
                            value={this.state.associacao.descricao}
                        />
                    </Content>
                }
            </Container>
        );
    }
}

export default AssociacaoScreen