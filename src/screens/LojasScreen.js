import React, { Component } from 'react';
import {
  Image,
  ScrollView, 
  View,
  Dimensions,
  TouchableHighlight
} from 'react-native'

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    Card, 
    CardItem, 
    Thumbnail,
    Spinner
  } from 'native-base';

import axios from "axios"
import config from "./../../app.json"


class LojasScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        lojas: [],
        isLoading: true,
        categoria: {}
    }
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const categoria = navigation.getParam('categoria', false)
    await this.setState({categoria: categoria})
    axios.get(`${config.api}/empresas-categoria/${categoria._id}`).then(r => {
      this.setState({ lojas: r.data, isLoading: false })
    })
  }
    render() {
        return (
          <Container>
            <Header>
              <Left />
              <Body>
                  <Title>Supermercados</Title>
              </Body>
              <Right/>
            </Header>
            {this.state.isLoading ? <Spinner /> :
              <Content>
                {this.state.lojas.map((l, i) => {
                  return (
                    <Card>
                      <CardItem>
                        <Left>
                          <Thumbnail source={{uri: `${config.base}empresas/${l.imagens[0]}`}} />
                          <Body>
                            <Text>{l.nome}</Text>
                            <Text note>Aberto</Text>
                          </Body>
                        </Left>
                      </CardItem>
                      <TouchableHighlight onPress={() => this.props.navigation.navigate('LojaScreen', {'loja': l})}>
                        <CardItem cardBody>
                          <Image source={{uri: `${config.base}empresas/${l.imagens[0]}`}} style={{height: 200, width: null, flex: 1}}/>
                        </CardItem>
                      </TouchableHighlight>
                    </Card>
                  )
                })}
                
                </Content>
            }
          </Container>
        );
    }
}

export default LojasScreen