import React, { Component } from 'react';

import { 
    Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    Text,
    Card,
    CardItem
  } from 'native-base';


import Carousel from './../components/Carousel'

class VerEventoScreen extends React.Component {
    constructor(props) {
        super(props)

    }
    render() {
        images = [ 
            { uri: 'https://eyechannel.com.br/wp-content/uploads/2018/05/concurso-matao-sp-2018.jpg'},
            { uri: 'https://eyechannel.com.br/wp-content/uploads/2018/05/concurso-matao-sp-2018.jpg'} 
        ]
        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>Evento</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <Carousel images={images} />
                    <Card>
                        <CardItem>
                            <Body>
                                <Text>Conheça melhor a cidade</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </Text>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default VerEventoScreen