import React, { Component } from 'react';

import { Container, Header, Content, List, ListItem, Text } from 'native-base';

import { View, Image } from 'react-native'

class SideBar extends Component {
    render() {
        return(
            <Container>
                <Content>
                    <Image 
                        source={{uri: 'https://kynesilverhide.github.io/images/banner_materialdesign.PNG'}}
                        style={{height: 150, width: null, flex: 1}}
                    />
                    <List>
                        <ListItem itemDivider>
                            <Text>Menu</Text>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('ProdutoScreen')}>
                            <Text>Produtos</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Lojas</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Fidelidades</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Entrar</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default SideBar