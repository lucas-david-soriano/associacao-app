import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk'

//import placeReducer from './reducers/placeReducer';
//import userReducer from './reducers/userReducer'
import produtoReducer from './reducers/produtoReducer'

const initialState = {}
const middleware   = [thunk]
let composeEnhancers

if(process.env.NODE_ENV === 'development')
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
else 
    composeEnhancers = compose

const rootReducer = combineReducers({
  produtos: produtoReducer,
});

const configureStore = () => {
  return createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(...middleware)));
}

export default configureStore;