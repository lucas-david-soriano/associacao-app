import React, { Component } from 'react';
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator, createStackNavigator, createDrawerNavigator } from "react-navigation";

import HomeScreen from './src/screens/HomeScreen'
import LojaScreen from './src/screens/LojaScreen'
import CategoriaScreen from './src/screens/CategoriaScreen'
import LojasScreen from './src/screens/LojasScreen'
import CidadeScreen from './src/screens/CidadeScreen'
import AssociacaoScreen from './src/screens/AssociacaoScreen'
import EventosScreen from './src/screens/EventosScreen'
import NoticiasScreen from './src/screens/NoticiasScreen'
import VerEventoScreen from './src/screens/VerEventoScreen'
import VerNoticiaScreen from './src/screens/VerNoticiaScreen'

import SideBar from './src/components/Sidebar'

const Drawer = DrawerNavigator({
  HomeScreen: {
    screen: HomeScreen
  },
  LojaScreen: {
    screen: LojaScreen
  },
  CategoriaScreen: {
    screen: CategoriaScreen
  },
  LojasScreen: {
    screen: LojasScreen
  },
  CidadeScreen: {
    screen: CidadeScreen
  }
}, {
  initialRouteName: "HomeScreen",
  contentComponent: (props) => <SideBar {...props} />
});

const AppNavigator = createStackNavigator({
  Drawer: {
    screen: Drawer
  },
  HomeScreen: {
    screen: HomeScreen
  },
  LojaScreen: {
    screen: LojaScreen
  },
  CategoriaScreen: {
    screen: CategoriaScreen
  },
  LojasScreen: {
    screen: LojasScreen
  },
  CidadeScreen: {
    screen: CidadeScreen
  },
  AssociacaoScreen: {
    screen: AssociacaoScreen
  },
  EventosScreen: {
    screen: EventosScreen
  },
  NoticiasScreen: {
    screen: NoticiasScreen
  },
  VerEventoScreen: {
    screen: VerEventoScreen
  },
  VerNoticiaScreen: {
    screen: VerNoticiaScreen
  }
}, {
  headerMode: 'none',
  initialRouteName: 'HomeScreen'
});

export default () =>
  <Root>
    <AppNavigator />
  </Root>;